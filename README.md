# ComputerCraft Scripts

Links for paste's of my ComputerCraft scripts.

## Scripts

| Script name        | Pastebin code                                | Platform     | Dependencies           | Description                                               |
| :----------------: | :------------------------------------------: | :----------: | :--------------------: | :-------------------------------------------------------: |
| startup.lua        | [4LVVF0QD](https://pastebin.com/4LVVF0QD)    | Any          | none                   | Makes the OS more UNIXy                                   |
| farm.lua           | [7b4AeF4C](https://pastebin.com/7b4AeF4C)    | Turtle       | none                   | A farming script for turtles                              |
| menu.lua           | [Jb6AQzD2](https://pastebin.com/Jb6AQzD2)    | Any          | none                   | An API for making simple menus                            |
| brody.lua          | [dGXQuFcv](https://pastebin.com/dGXQuFcv)    | Any          | menu.lua               | A simple message broadcast software                       |
| weeber.lua         | [aU52DBSA](https://pastebin.com/aU52DBSA)    | Any          | none                   | A global anonymous chat app                               |
| nuker.lua          | [affsgqXR](https://pastebin.com/affsgqXR)    | Turtle       | none                   | Goes forward and drops tnt (don't put close to ground)    |
| libcartesian.lua   | [W2e0t4he](https://pastebin.com/W2e0t4he)    | Turtle       | none                   | A library for moving a turtle in the 3D space             |
